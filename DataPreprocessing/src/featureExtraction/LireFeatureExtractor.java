package featureExtraction;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.imageio.ImageIO;


import net.semanticmetadata.lire.imageanalysis.AutoColorCorrelogram;
import net.semanticmetadata.lire.imageanalysis.BasicFeatures;
import net.semanticmetadata.lire.imageanalysis.CEDD;
import net.semanticmetadata.lire.imageanalysis.ColorLayout;
import net.semanticmetadata.lire.imageanalysis.EdgeHistogram;
import net.semanticmetadata.lire.imageanalysis.FCTH;
import net.semanticmetadata.lire.imageanalysis.FuzzyOpponentHistogram;
import net.semanticmetadata.lire.imageanalysis.Gabor;
import net.semanticmetadata.lire.imageanalysis.JointHistogram;
import net.semanticmetadata.lire.imageanalysis.JointOpponentHistogram;
import net.semanticmetadata.lire.imageanalysis.ScalableColor;
import net.semanticmetadata.lire.imageanalysis.SimpleColorHistogram;
import net.semanticmetadata.lire.imageanalysis.Tamura;
import net.semanticmetadata.lire.utils.FileUtils;



public class LireFeatureExtractor {

	public static void main(String args[]) {
		
//		if(args.length<2)
//		{
//			System.err.format("Usage: [input dir] [output file] [delete - if the image file should be deleted once read]%n");
//			System.exit(1);
//		}
		String indir = "E:/PlacingTaskData/GeoTaggedImages";
		String outfile= "E:/PlacingTaskData/GeoTaggedImages/Features.txt";
		
		boolean delete = false;
//		if(args.length>=3 && args[2].equals("delete"))
//		{
//			delete=true;
//			System.err.println("WARNING: will delete images upon reading!");
//		}

		AutoColorCorrelogram acc = new AutoColorCorrelogram();
		BasicFeatures bf = new BasicFeatures();
		CEDD cedd = new CEDD();
		ColorLayout col = new ColorLayout();
		EdgeHistogram edge = new EdgeHistogram();
		FCTH fcth = new FCTH();
		FuzzyOpponentHistogram fop = new FuzzyOpponentHistogram();
		Gabor gab = new Gabor();
		JointHistogram jh = new JointHistogram();
		JointOpponentHistogram jop = new JointOpponentHistogram();
		ScalableColor sc = new ScalableColor();
		SimpleColorHistogram sch = new SimpleColorHistogram();
		Tamura tam = new Tamura();

		try {
			
			HashSet<String> processedFiles = new HashSet<String>();

				File f = new File(outfile);
				if(f.exists())
				{
					System.err.println("Outfile "+outfile+" already exists, will append!");
					BufferedReader br = new BufferedReader(new FileReader(outfile));
					String line="";
					while((line=br.readLine())!=null)
					{
						String fileDone = new String(line.split("\\s")[0]);
						processedFiles.add(fileDone);
						if(processedFiles.size()<5)
							System.err.println("adding to processed files: "+fileDone);
						
						if(processedFiles.size()%10000==0)
							System.err.println("Number of files read: "+processedFiles.size());
					}
					br.close();
					System.err.println("Number of entries read from "+outfile+": "+processedFiles.size());
				}

			BufferedWriter bw = new BufferedWriter(new FileWriter(outfile,true));

			System.err.println("Reading files from " + indir);
			ArrayList<File> images = FileUtils.getAllImageFiles(
					new File(indir), true);// descend into subdirectories

			StringBuffer sb = new StringBuffer();

			int numImages = 0;
			
			for (File image : images) {

				char c = image.getName().charAt(0);
				if (!Character.isDigit(c)) {
					System.err.println("ignoring " + image.toString());
					continue;
				}
				sb.setLength(0);

				numImages++;
				
				if( processedFiles.contains(image.getName()))
				{
					System.err.println("Skipping "+image.getName());
					continue;
				}

				System.err.format(
						"Features extracted for %d out of %d images%n",
						numImages, images.size());

				try {
					sb.append(image.getName());
					BufferedImage bim = ImageIO.read(image);

					acc.extract(bim);
					sb.append(" acc " + acc.getStringRepresentation());

					bf.extract(bim);
					sb.append(" bf " + bf.getStringRepresentation());

					cedd.extract(bim);
					sb.append(" " + cedd.getStringRepresentation());

					col.extract(bim);
					sb.append(" col " + col.getStringRepresentation());

					edge.extract(bim);
					sb.append(" " + edge.getStringRepresentation().replace(";", " "));

					fcth.extract(bim);
					sb.append(" " + fcth.getStringRepresentation());

					fop.extract(bim);
					sb.append(" " + fop.getStringRepresentation());

					gab.extract(bim);
					sb.append(" " + gab.getStringRepresentation());

					jh.extract(bim);
					sb.append(" " + jh.getStringRepresentation());

					jop.extract(bim);
					sb.append(" " + jop.getStringRepresentation());

					sc.extract(bim);
					sb.append(" " + sc.getStringRepresentation().replace(";", " "));

					sch.extract(bim);
					sb.append(" " + sch.getStringRepresentation());

					tam.extract(bim);
					sb.append(" " + tam.getStringRepresentation());

					bw.write(sb.toString());
					bw.newLine();
					
					
					if(delete==true)
					{
						image.delete();
					}
					
					
					if(numImages%100==0)
					{
						bw.flush();
					}
					
				} catch (Exception e) {
					System.err.format("Error at image %d%n", numImages);
					continue;
				}
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
}
