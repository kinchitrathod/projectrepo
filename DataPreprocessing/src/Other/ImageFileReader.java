/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Other;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 *
 * @author KINCHIT RATHOD
 */
public class ImageFileReader {
    
     private static final String INPUT_FILE1 = "E:\\PlacingTaskData\\ProblemOutput.txt";
    private static final String INPUT_FILE2 = "E:\\PlacingTaskData\\ImageFile1\\imagefeatures_1";
    private static final String OUTPUTFILE = "E:\\PlacingTaskData\\ImageFeatures6Comparison.txt";
    
    public static void main(String args[])
    {
        try {
            
             boolean success1 = (new File(OUTPUTFILE)).delete();
            if (success1) 
            System.out.println("Output File Deleted!");
            
             BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE1));
             BufferedReader b = new BufferedReader(new FileReader(INPUT_FILE2));
             String lineStr1 = null;
             String lineStr2 = null;

             FileOutputStream fileOutput = new FileOutputStream(OUTPUTFILE);
             PrintStream outputPrint = new PrintStream(fileOutput);
             
             lineStr1 = br.readLine();
             String array1[] = lineStr1.split(" ");
             
             
             lineStr2 = b.readLine();
             String array2[]  = lineStr2.split(" ");
            
             outputPrint.println("\t" + array1[0]+"\t"+array2[0]);
             for(int i =1 ; i<array1.length;i++)
             {
          //       System.out.println(i +" : "+array2[i]);
             outputPrint.println(i+"\t"+array1[i]+"\t"+array2[i]);
             }
            }
           catch(Exception e)
           {
           }
    }
}
