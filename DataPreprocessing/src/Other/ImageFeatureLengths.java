/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Other;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author KINCHIT RATHOD
 */
public class ImageFeatureLengths {
  
    private static final String INPUT_FILE1 = "H:/Placing Task Data/ImageFeaturesFiltered/imagefeatures_EDGEHISTOGRAM_9.txt";
    private static final String INPUT_FILE2 = "C:/Users/KINCHIT RATHOD/Documents/NetBeansProjects/DataPreprocessing/src/resources/imagefeatures_COL_9.txt";
    
    public static void main(String args[])
    {
       try {
       
            BufferedReader in = new BufferedReader(new FileReader(INPUT_FILE1));
            String str =in.readLine();
            String inStr[] = str.split(" ");            
            System.out.println("Edgehistogram Length: "+inStr.length);
            
            BufferedReader inCol = new BufferedReader(new FileReader(INPUT_FILE2));
            String colStr =inCol.readLine();
            String inColStr[] = colStr.split(" ");
            System.out.println("Col Length: "+inColStr.length);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
}
