/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Other;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author KINCHIT RATHOD
 */
public class ClustersFilter {
    
    private static final String INPUT_FILE = "E:\\PlacingTaskData\\Demo\\Clusters.txt";
    private static final String OUTPUT_FILE = "E:\\PlacingTaskData\\ClustersFiltered.txt";
    private static final String REGEX = "^6+[0-9]*$";
    
    public static void main(String args[])
    {
        boolean success = (new File(OUTPUT_FILE)).delete();
        if (success) 
            System.out.println("ClustersFiltered File Deleted!");
        
        try {
             BufferedReader iClusterReader = new BufferedReader(new FileReader(INPUT_FILE));
             String lineStr = null;
             int imageCount=0;
             Pattern p = Pattern.compile(REGEX);
             
             while((lineStr = iClusterReader.readLine())!=null)
             {
                 String lineSplit[] = null;
                 lineSplit = lineStr.split(" ");
                 String[] imageIDSplits = lineStr.substring(lineStr.indexOf(" ") + 1).split(",");
                for (String imageIDSplit : imageIDSplits) {
                    //System.out.println(imageIDSplit);
                    String imageID = imageIDSplit.substring(0, imageIDSplit.indexOf(" "));
                    //System.out.println(imageID);
                    Matcher m = p.matcher(imageID);
                    while (m.find()) {
                        {
                            imageCount++;
                            System.out.println(imageIDSplit);
                            System.out.println(imageID);
                            
                        }
                        
                    }
                }
             }
             
             System.out.println("Images:"+imageCount);
        } catch (Exception e) {
        }
    }
}
