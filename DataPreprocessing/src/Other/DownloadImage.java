/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Other;

import static Other.File6Analysis.DownloadImageClass.saveImage;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author KINCHIT RATHOD
 */
public class DownloadImage {
    
    private static final String INPUT_FILE= "E:/PlacingTaskData/RetrievalModel.txt";
    private static final String INPUT_FILE2 = "E:/PlacingTaskData/Metadata/metadata_1/metadata_1.csv";
    
    public static void main(String args[])
    {
        try {
             BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE));
             String lineStr = null;
             Set<String> imageSet = new HashSet<>();
             
             int count1=0;
             
            while((lineStr=br.readLine())!=null)
            {
                String queryImage = lineStr.substring(0, lineStr.indexOf(" "));
                boolean added = imageSet.add(queryImage);
               
                if (added) {
                    System.out.println("True");
                } else {
                    System.out.println("False");
                }
                count1++;
            }
            
            System.out.println("Count 1:"+count1);
            
            br.close();
            
            BufferedReader Reader = new BufferedReader(new FileReader(INPUT_FILE));
            lineStr = null;
            String[] imageIDSplits= null;
            String line=null;
            while((lineStr=Reader.readLine())!=null)
            {
                String queryLine = lineStr.substring(0, lineStr.indexOf(":"));
                imageIDSplits = lineStr.substring(lineStr.indexOf(":")+1).split(",");
                for (String imageIDSplit : imageIDSplits) 
                {
                    imageSet.add(imageIDSplit);
                }
            }
            
            System.out.println("Image Set:"+imageSet.size());
            
            Reader.close();
            
            BufferedReader BReader = new BufferedReader(new FileReader(INPUT_FILE2));
            
            for (String s : imageSet) {
                String metadata = null;
                String lineSplit[] = null;
                String imageUrl = null;
                String destinationFile = null;
                while((metadata=BReader.readLine())!=null)
                {
                    String imageID= null;
                    imageID= metadata.substring(0, metadata.indexOf(","));
                    if(imageSet.contains(imageID)){
                        lineSplit = metadata.split(",");
                        System.out.println(lineSplit[0] +" "+lineSplit[3]);
                        imageUrl = lineSplit[3];
                        destinationFile = "E:/PlacingTaskData/ModelImages/"+lineSplit[0]+".jpg";
                        saveImage(imageUrl, destinationFile);
                        System.out.println("Image "+lineSplit[0]+" Downloaded");
                        break;
                    }
                }
            }
            
        } catch (Exception e) {
        }
        
    }
    
         public static void saveImage(String imageUrl, String destinationFile) throws IOException {
            URL url = new URL(imageUrl);
            InputStream is = url.openStream();
            OutputStream os = new FileOutputStream(destinationFile);
                      
            byte[] b = new byte[2048];
            int length;

            while ((length = is.read(b)) != -1) {
                    os.write(b, 0, length);
            }

            is.close();
            os.close();
        }
    
}
