/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Other;

import java.io.IOException;
import java.io.FileReader;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author KINCHIT RATHOD
 */
public class GeoTaggedFilterData {
    private static final String INPUT_FILE="E:\\PlacingTaskData\\Demo\\Test.csv";
    private static final String OUTPUT_FILE1="E:\\PlacingTaskData\\Demo\\Test1_tagged.csv";
    
    private static final String REGEX = "(geo:lat=|geolat)";
    
    public static void main(String args[]) throws IOException
    {
       CSVReader reader = new CSVReader(new FileReader(INPUT_FILE));
       CSVWriter  taggedwriter = new CSVWriter(new FileWriter(OUTPUT_FILE1));
      
        long count1=0, count2=0, totalcount=0;
        try {
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                 totalcount++;
                String INPUT = nextLine[4];
                     Pattern p = Pattern.compile(REGEX);
                    Matcher m = p.matcher(INPUT); 
                    while(m.find()) {
                        count1++;
                         taggedwriter.writeNext(nextLine);
                        }
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.toString());
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        finally
        {
             taggedwriter.close();
             reader.close();
        }
        System.out.println("Total Records Counted For: " + totalcount);
         System.out.println("Records Tagged: "+count1);
         
    }
}
