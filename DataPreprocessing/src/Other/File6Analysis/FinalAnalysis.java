/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Other.File6Analysis;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KINCHIT RATHOD
 */
public class FinalAnalysis {
    
    private static final String INPUT_FILE1 = "E:/PlacingTaskData/ImageFile6/AnalysisClusteredImages.txt";
    private static final String INPUT_FILE2  = "E:/PlacingTaskData/ImageFile6/AnalysisMissingImagesFeaturesNew.txt";
    private static final String INPUT_FILE3  = "E:/PlacingTaskData/ImageFile6/metadata_6.csv";
    private static final String INPUT_FILE4  = "E:/PlacingTaskData/ImageFile6/imagefeatures_6NoFullFeature.txt";
    private static final String OUTPUT_FILE1 = "E:/PlacingTaskData/ImageFile6/AnalysisTagged.txt";
    private static final String OUTPUT_FILE2 = "E:/PlacingTaskData/ImageFile6/AnalysisUnTagged.txt";
    private static final String OUTPUT_FILE3 = "E:/PlacingTaskData/ImageFile6/AnalysisImageMetadata.txt";
    private static final String OUTPUT_FILE4 = "E:/PlacingTaskData/ImageFile6/AnalysisImageFeatures.txt";
    
    public static void main(String args[]) throws FileNotFoundException
    {
        try {
            BufferedReader clusterReader = new BufferedReader(new FileReader(INPUT_FILE1));
            String lineStr = null;
            int  tagged=0,untagged=0;
            List<String> imageList = new ArrayList<>();
            
            FileOutputStream fileOutput1 = new FileOutputStream(OUTPUT_FILE1, true);
            PrintStream outputPrintImagesTagged = new PrintStream(fileOutput1);
           
             FileOutputStream fileOutput2 = new FileOutputStream(OUTPUT_FILE2, true);
            PrintStream outputPrintImagesUnTagged = new PrintStream(fileOutput2);
            
             FileOutputStream fileOutput3 = new FileOutputStream(OUTPUT_FILE3, true);
            PrintStream outputPrintImagesMetadata = new PrintStream(fileOutput3);
           
            FileOutputStream fileOutput4 = new FileOutputStream(OUTPUT_FILE4, true);
            PrintStream outputPrintImagesFeatures = new PrintStream(fileOutput4);
            
            while ((lineStr = clusterReader.readLine()) != null) {
                imageList.add(lineStr);
            }
            
            BufferedReader imageReader = new BufferedReader(new FileReader(INPUT_FILE2));
            
            List<String> taggedImages = new ArrayList<>();
            
            while ((lineStr = imageReader.readLine()) != null) {
                String imageID = lineStr.substring(0, lineStr.indexOf(" "));
                
                if (imageList.contains(imageID)) {
                    tagged++;
                    outputPrintImagesTagged.println(lineStr);
                    taggedImages.add(imageID);
                } else {
                    untagged++;
                    outputPrintImagesUnTagged.println(lineStr);
                }
            }
            
            
            
            System.out.println("Tagged: "+tagged);
            System.out.println("UnTagged: " +untagged);
            
            System.out.println(taggedImages.size());
            
            BufferedReader metadataReader = new BufferedReader(new FileReader(INPUT_FILE3));
            String str = null;
            int count=0;
            
            while ((str = metadataReader.readLine()) != null) {
                String imageID = str.substring(0, str.indexOf(","));
               // System.out.println(imageID);
                if (taggedImages.contains(imageID)) {
                    outputPrintImagesMetadata.println(str);
                    count++;
                } 
            }
            
            System.out.println("Count: "+count);
            
            BufferedReader featureReader = new BufferedReader(new FileReader(INPUT_FILE4));
            str = null; 
            count =0;
            
            while ((str = featureReader.readLine()) != null) {
                String imageID = str.substring(0, str.indexOf(" "));
               // System.out.println(imageID);
                if (taggedImages.contains(imageID)) {
                    outputPrintImagesFeatures.println(str);
                    count++;
                } 
            }
            
            System.out.println("Count: "+count);
            
        } catch (IOException ex) {
            Logger.getLogger(FinalAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
