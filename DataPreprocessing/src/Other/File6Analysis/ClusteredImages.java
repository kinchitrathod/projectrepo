/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Other.File6Analysis;

import DataPreprocessing.GeoTaggedFinder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KINCHIT RATHOD
 */
public class ClusteredImages {
    
    private static final String INPUT_FILE = "E:\\PlacingTaskData\\SortedGeoTags.txt";
     private static final String OUTPUT_FILE = "E:\\PlacingTaskData\\ImageFile6\\AnalysisClusteredImages.txt";
    public static void main(String args[]) throws FileNotFoundException
    {
        BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE));
        String lineStr = null;
        int TotalImageCounts=0;
        int clusteredImageCounts=0;
        
        try {
            
            FileOutputStream outputFile = new FileOutputStream(OUTPUT_FILE);
            PrintStream printOutputFile = new PrintStream(outputFile);
            
            printOutputFile.print("No. of Clustered Images from File 6 and their ImageIDs");
            printOutputFile.println();
            while((lineStr=br.readLine()) != null)
            {
                String imageID = lineStr.substring(0, lineStr.indexOf(" "));
                if(imageID.startsWith("6"))
                {
                    clusteredImageCounts++;
                    printOutputFile.println(imageID);
                }
                //System.out.println(imageID);
                
                TotalImageCounts++;
                
            }
            
            System.out.println("Total Image Counts:"+TotalImageCounts);
            System.out.println("Total Clustered Images From File 6:"+clusteredImageCounts);
             printOutputFile.println("Total Clustered Images From File 6:"+clusteredImageCounts);
            
            for (int i = 0; i < 3; i++)
        {
                try {
                    Thread.sleep(300);
                    Other.Beep.makeasound();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeoTaggedFinder.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(FileAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
