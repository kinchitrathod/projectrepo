/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Other.File6Analysis;

import DataPreprocessing.GeoTaggedFinder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KINCHIT RATHOD
 */
public class FileAnalysis {
    
    private static final String INPUT_FILE = "E:\\PlacingTaskData\\Images\\Features.txt";
    //private static final String OUTPUT_FILE = "E:\\PlacingTaskData\\ImageFile6\\AnalysisFeatureStatistics.txt";
    //private static final String OUTPUT_FILE1 = "E:\\PlacingTaskData\\ImageFile6\\AnalysisMissingImagesFeatures.txt";
    public static void main(String args[]) throws FileNotFoundException
    {
        BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE));
        String lineStr = null;
        
//        FileOutputStream outputFile = new FileOutputStream(OUTPUT_FILE);
//        PrintStream printOutputFile = new PrintStream(outputFile);
//        
//        FileOutputStream outputFile1 = new FileOutputStream(OUTPUT_FILE1);
//        PrintStream printOutputFeaturesFile = new PrintStream(outputFile1);
        
        int totalImageCounts=0;
        int imageWithFeatures=0;
        int imageWithNoFeatures=0;
        
        
        try {
            
            while((lineStr=br.readLine()) != null)
            {
                totalImageCounts++;
                String lineSplit[]=lineStr.split(" ");
                if(lineSplit.length<=3973)
                {
                    imageWithNoFeatures++;
                    //printOutputFile.println(lineSplit[0] + " "+lineSplit.length);
                    //printOutputFeaturesFile.println(lineStr);
                    System.out.println("ImageID:"+lineSplit[0] +" "+ "Length:"+lineSplit.length);
                    
                }
                else
                {
                    imageWithFeatures++;
                    System.out.println("ImageID:"+lineSplit[0] +" "+ "Length:"+lineSplit.length);
                }
            }
            
            System.out.println("Total Image Counts:"+totalImageCounts);
            System.out.println("Images with features:"+imageWithFeatures);
            System.out.println("Images with NO features:"+imageWithNoFeatures);
            
            for (int i = 0; i < 3; i++)
        {
                try {
                    Thread.sleep(300);
                    Other.Beep.makeasound();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeoTaggedFinder.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(FileAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
}
