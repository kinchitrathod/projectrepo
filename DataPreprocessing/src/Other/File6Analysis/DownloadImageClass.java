/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Other.File6Analysis;

import DataPreprocessing.GeoTaggedFinder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KINCHIT RATHOD
 */
public class DownloadImageClass {
    
    //private static final String INPUT_FILE= "E:/PlacingTaskData/ImageFile6/AnalysisImageMetadata.txt";  // Set input file path for GeoTagRefined.txt file 
    private static final String DIRECTORY_PATH = "E:/PlacingTaskData/GeoTaggedImages/"; // Set download location directory path here
    private static final String INPUT_FILE= "E:/PlacingTaskData/GeoTagRefined.csv";  // Set input file path for GeoTagRefined.txt file 
    
    public static void main(String args[]) throws IOException
    {
        
        BufferedReader metadataReader = new BufferedReader(new FileReader(INPUT_FILE));
        String lineStr = null;
        
        while((lineStr=metadataReader.readLine())!=null)
        {
            String lineSplit[] = null;
            lineSplit = lineStr.split(",");
        
            String imageUrl = lineSplit[3];
            String destinationFile = DIRECTORY_PATH+lineSplit[0]+".jpg";

            File f  = new File(destinationFile);
            
            if(f.exists()){
		  System.out.println("1");
	  }else{
                                System.out.println("2");
		  saveImage(imageUrl, destinationFile);
	  }
            
            //saveImage(imageUrl, destinationFile);
            //System.out.println("Image "+lineSplit[0]+" Downloaded");
        }
         for (int i = 0; i < 3; i++) {
                try {
                    Thread.sleep(300);
                    Other.Beep.makeasound();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeoTaggedFinder.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        
    }
    
        public static void saveImage(String imageUrl, String destinationFile) throws IOException {
            URL url = new URL(imageUrl);
            InputStream is = url.openStream();
            OutputStream os = new FileOutputStream(destinationFile);

            byte[] b = new byte[2048];
            int length;

            while ((length = is.read(b)) != -1) {
                    os.write(b, 0, length);
            }

            is.close();
            os.close();
        }
    
}
