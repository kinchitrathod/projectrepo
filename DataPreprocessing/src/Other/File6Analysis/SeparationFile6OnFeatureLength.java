/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Other.File6Analysis;

import DataPreprocessing.GeoTaggedFinder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KINCHIT RATHOD
 */
public class SeparationFile6OnFeatureLength {
    
    private static final String INPUT_FILE = "E:\\PlacingTaskData\\ImageFile6\\imagefeatures_6";
    private static final String OUTPUT_FILE1 = "E:\\PlacingTaskData\\ImageFile6\\imagefeatures_6FullFeature.txt";
    private static final String OUTPUT_FILE2 = "E:\\PlacingTaskData\\ImageFile6\\imagefeatures_6NoFullFeature.txt";
    private static final String OUTPUT_FILE3 = "E:\\PlacingTaskData\\ImageFile6\\AnalysisMissingImagesFeaturesNew.txt";
  
    public static void main(String args[]) throws FileNotFoundException
    {
        BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE));
        String lineStr = null;
        
        FileOutputStream outputFile1 = new FileOutputStream(OUTPUT_FILE1);
        PrintStream printOutputFullFeaturesFile = new PrintStream(outputFile1);
        
        FileOutputStream outputFile2 = new FileOutputStream(OUTPUT_FILE2);
        PrintStream printOutputNoFullFeaturesFile = new PrintStream(outputFile2);
        
        FileOutputStream outputFile3 = new FileOutputStream(OUTPUT_FILE3);
        PrintStream printOutput = new PrintStream(outputFile3);
        
        int totalImageCounts=0;
        int imageWithFeatures=0;
        int imageWithNoFeatures=0;
        
        try {
                
             while((lineStr=br.readLine()) != null)
            {
                totalImageCounts++;
                String lineSplit[]=lineStr.split(" ");
                if(lineSplit.length == 3973)
                {
                    imageWithFeatures++;
                    printOutputFullFeaturesFile.println(lineStr);
                    //System.out.println("ImageID:"+lineSplit[0] +" "+ "Length:"+lineSplit.length);
                    
                }
                else
                {
                    imageWithNoFeatures++;
                    System.out.println("Found: "+lineSplit.length);
                    printOutputNoFullFeaturesFile.println(lineStr);
                    printOutput.println(lineSplit[0]+" "+lineSplit.length);
                }
            }
            
            System.out.println("Total Image Counts:"+totalImageCounts);
            System.out.println("Images with features:"+imageWithFeatures);
            System.out.println("Images with NO features:"+imageWithNoFeatures);
            
            for (int i = 0; i < 3; i++)
            {
                try {
                    Thread.sleep(300);
                    Other.Beep.makeasound();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeoTaggedFinder.class.getName()).log(Level.SEVERE, null, ex);
                }
              }
             
             
        } catch (Exception e) {
        }
        
    }
    
}
