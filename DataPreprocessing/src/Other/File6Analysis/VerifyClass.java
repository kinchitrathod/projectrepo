/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Other.File6Analysis;

import Other.Beep;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KINCHIT RATHOD
 */
public class VerifyClass {
    
    private static final String INPUT_FILE1 ="E:\\PlacingTaskData\\GeoTagRefined.csv";
    private static final String INPUT_FILE2 ="E:\\PlacingTaskData\\COLFeaturesRefined.txt";
    
    public static void main(String args[])
    {
        
        try {
 
           BufferedReader geoTagReader = new BufferedReader(new FileReader(INPUT_FILE1));
           String lineStr = null;
           String imageID= null;
           
           List<String> imageList = new ArrayList<>();
           
           int count1=0;
           
           while((lineStr=geoTagReader.readLine())!=null)
           {
               imageID = lineStr.substring(0, lineStr.indexOf(","));
               //System.out.println(imageID);
               imageList.add(imageID);
               count1++;
           }

           System.out.println("Count1:"+count1);
           geoTagReader.close();
            
           BufferedReader featureReader = new BufferedReader(new FileReader(INPUT_FILE2));
           lineStr=null;
           
           Beep.makeasound();
           count1=0;
           int count2=0;
           
           while((lineStr=featureReader.readLine())!=null)
           {
               String ImageID= lineStr.substring(0, lineStr.indexOf(" "));
                if(imageList.contains(ImageID))
                {
                    imageList.remove(ImageID);
                    //System.out.println(imageList.size());
                    count1++;
                }
                else{
                    System.out.println("ImageID:"+ImageID+" Not found");
                    count2++;
                }
           }
           System.out.println("Found:"+count1);
           System.out.println("Not Found:"+count2);
            
           for (String s : imageList) {
               System.out.println(s);
               
           }
               
           
            for (int n = 0; n < 3; n++)
            {
                try {
                    Thread.sleep(300);
                    Beep.makeasound();
                } catch (InterruptedException ex) {
                    Logger.getLogger(VerifyClass.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            
            
        } catch (Exception e) {
        }
        
    }
}
