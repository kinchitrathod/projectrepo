/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Other.File6Analysis;

import DataPreprocessing.GeoTaggedFinder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KINCHIT RATHOD
 */
public class FeatureExtractionFile6 {

    private static final String INPUT_FILE = "E:\\PlacingTaskData\\Images\\Features.txt";
    private static final String OUTPUTFILE1 = "E:\\PlacingTaskData\\ImageFile6\\ImageFeatureRemainingCOLExtracted.txt";
    private static final String OUTPUTFILE2 = "E:\\PlacingTaskData\\ImageFile6\\ImageFeatureRemainingEHDExtracted.txt";

    public static void main(String args[]) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE));
        String lineStr = null;
        int count=0;
        
        FileOutputStream fileOutputCOL = new FileOutputStream(OUTPUTFILE1);
        PrintStream outputPrintCOL = new PrintStream(fileOutputCOL);
        FileOutputStream fileOutputEHD = new FileOutputStream(OUTPUTFILE2);
        PrintStream outputPrintEHD = new PrintStream(fileOutputEHD);

        BufferedReader IBufferedReader = new BufferedReader(new FileReader(INPUT_FILE));

        while ((lineStr = IBufferedReader.readLine()) != null)
        {
            
            
            StringBuilder col = new StringBuilder();
            StringBuilder ehd = new StringBuilder();

            String photoID = lineStr.substring(0, lineStr.indexOf("acc")).trim();
            System.out.println("PhotoID:" + photoID);

            col.append(lineStr.substring((lineStr.indexOf("col")), lineStr.indexOf("edgehistogram")).trim());
            outputPrintCOL.println(photoID + " " + col.toString());

            ehd.append(lineStr.substring((lineStr.indexOf("edgehistogram")), lineStr.indexOf("fcth")).trim());
            outputPrintEHD.println(photoID + " " + ehd.toString());
            count++;
        }
        
        System.out.println("Total Records Extracted:"+count);
        
        for (int i = 0; i < 3; i++)
        {
                try {
                    Thread.sleep(300);
                    Other.Beep.makeasound();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeoTaggedFinder.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        
    }
}
