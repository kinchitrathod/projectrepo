/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DataPreprocessing;

/**
 *
 * @author KINCHIT RATHOD
 */
public class DOCluster   {
    
    private long clusterID;
    DOGeoTagPhoto[] doGeoTag;

    public DOGeoTagPhoto[] getDoGeoTag() {
        return doGeoTag;
    }

    public void setDoGeoTag(DOGeoTagPhoto[] doGeoTag) {
        this.doGeoTag = doGeoTag;
    }

    
    public long getClusterID() {
        return clusterID;
    }

    public void setClusterID(long clusterID) {
        this.clusterID = clusterID;
    }

   
}
