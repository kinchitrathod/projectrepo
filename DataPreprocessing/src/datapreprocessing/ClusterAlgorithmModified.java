/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataPreprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author KINCHIT RATHOD
 */
public class ClusterAlgorithmModified {

    private static final String INPUT_FILE1 = "E:\\PlacingTaskData\\SortedGeoTags.txt";
    private static final String CLUSTERS = "E:\\PlacingTaskData\\ClusterFiles\\Clusters.txt";
    private static final String CLUSTER_STATS = "E:\\PlacingTaskData\\ClusterFiles\\ClusterStatistics.txt";
    private static final String CLUSTER_IMAGESONLY = "E:\\PlacingTaskData\\ClusterFiles\\ClusterImagesOnly.txt";
    private static final String CLUSTER_FILTERED = "E:\\PlacingTaskData\\ClusterFiles\\ClusterFiltered.txt";
    private static final String REGEX = "^6+[0-9]*$";
    static long clusterId = 1;

    public static void main(String args[]) {

         boolean success1 = (new File(CLUSTERS)).delete();
        if (success1) 
            System.out.println("Clusters File Deleted!");
        boolean success2 = (new File(CLUSTER_STATS)).delete();
        if(success2)
            System.out.println("ClusterImagesOnly File Deleted!");
        boolean success3 = (new File(CLUSTER_IMAGESONLY)).delete();
        if(success3)
            System.out.println("ClustersFiltered File Deleted!");
        boolean success4 = (new File(CLUSTER_FILTERED)).delete();
        if(success4)
            System.out.println("ClusterStats File Deleted!");
        
        try {

            BufferedReader geoLocationSorted = new BufferedReader(new FileReader(INPUT_FILE1));
            String one = null;
            String two = null;
            one = geoLocationSorted.readLine();
            int thresholdDistance = 100;

            FileOutputStream fileOutput = new FileOutputStream(CLUSTERS, true);
            PrintStream outputPrint = new PrintStream(fileOutput);
            
            FileOutputStream fileOutputStats = new FileOutputStream(CLUSTER_STATS, true);
            PrintStream outputPrintStats = new PrintStream(fileOutputStats);
            
            FileOutputStream fileOutputImagesOnly = new FileOutputStream(CLUSTER_IMAGESONLY, true);
            PrintStream outputPrintImages = new PrintStream(fileOutputImagesOnly);
            
            FileOutputStream fileClusterFilter = new FileOutputStream(CLUSTER_FILTERED, true);
            PrintStream outputPrintClusterFilter = new PrintStream(fileClusterFilter);
            
             Pattern p = Pattern.compile(REGEX);
            
            // Calculate and map the no. of images in each clusters
            HashMap<Long,Integer> clusterMap = new HashMap<>(); 
            
            
            List<DOCluster> clusterLists = new ArrayList<>();
            DOCluster[] clusterArray = new DOCluster[0];

            List<DOGeoTagPhoto> photoIDList=new ArrayList<>();
            DOGeoTagPhoto[] photoArray=new DOGeoTagPhoto[0];

            DOCluster iDOCluster=new DOCluster();
            iDOCluster.setClusterID(clusterId);
            
            String[] oneSplit=one.split(" ");
            String[] twoSplit;
            DOGeoTagPhoto iDOGeoTagPhoto=new DOGeoTagPhoto();
            iDOGeoTagPhoto.setLattitude(Double.parseDouble(oneSplit[1]));
            iDOGeoTagPhoto.setLongitude(Double.parseDouble(oneSplit[2]));
            iDOGeoTagPhoto.setPhotoID(Integer.parseInt(oneSplit[0]));
            photoIDList.add(iDOGeoTagPhoto);
            
            while ((two = geoLocationSorted.readLine()) != null) {
                oneSplit = one.split(" ");
                twoSplit = two.split(" ");

                double lat1 = Double.parseDouble(oneSplit[1]);
                double lon1 = Double.parseDouble(oneSplit[2]);

                double lat2 = Double.parseDouble(twoSplit[1]);
                double lon2 = Double.parseDouble(twoSplit[2]);

                HaversineFormula dis = new HaversineFormula();
                double distance = dis.haversine(lat1, lon1, lat2, lon2);
                //System.out.println("lat lon 1: " + one + " lat lon 2: " + two + " Dist: " + distance);
                
                if(distance<=thresholdDistance) {
                    DOGeoTagPhoto iDOGeoTagPhotoTwo=new DOGeoTagPhoto();
                    iDOGeoTagPhotoTwo.setLattitude(Double.parseDouble(twoSplit[1]));
                    iDOGeoTagPhotoTwo.setLongitude(Double.parseDouble(twoSplit[2]));
                    iDOGeoTagPhotoTwo.setPhotoID(Long.parseLong(twoSplit[0]));
                    photoIDList.add(iDOGeoTagPhotoTwo);
                } else {
                    photoArray=(DOGeoTagPhoto[]) photoIDList.toArray(photoArray);
                    iDOCluster.setDoGeoTag(photoArray);
                    clusterLists.add(iDOCluster);
                    photoArray=new DOGeoTagPhoto[0];
                    iDOCluster=new DOCluster();
                    clusterId++;
                    iDOCluster.setClusterID(clusterId);
                    photoIDList=new ArrayList<>();
                    DOGeoTagPhoto iDOGeoTagPhotoTwo=new DOGeoTagPhoto();
                    iDOGeoTagPhotoTwo.setLattitude(Double.parseDouble(twoSplit[1]));
                    iDOGeoTagPhotoTwo.setLongitude(Double.parseDouble(twoSplit[2]));
                    iDOGeoTagPhotoTwo.setPhotoID(Long.parseLong(twoSplit[0]));
                    photoIDList.add(iDOGeoTagPhotoTwo);
                }
                one=two;
            }

            
             long clusterCount=0;
            int photoIDCounts=0;
            for(DOCluster clusterList:clusterLists ) {
                DOGeoTagPhoto[] photoTagArray=clusterList.getDoGeoTag();
                //System.out.println("ClusterID:"+clusterList.getClusterID());
                clusterCount++;
                outputPrintStats.print(clusterList.getClusterID()+" ");
                
                outputPrint.print(clusterList.getClusterID()+" ");
                for(DOGeoTagPhoto photoTag:photoTagArray) {
                    photoIDCounts++;
                    outputPrint.print(photoTag.getPhotoID()+" "+photoTag.getLattitude()+" "+photoTag.getLongitude()+"," );
                    outputPrintImages.print(photoTag.getPhotoID()+" ");
                }
                outputPrintImages.println();
               //System.out.println(photoIDCounts);
               outputPrintStats.print(photoIDCounts);
               clusterMap.put(clusterList.getClusterID(), photoIDCounts);
               outputPrintStats.println();
               photoIDCounts=0; 
               outputPrint.println();
            }
            System.out.println();
            System.out.println("Total No Of Clusters: "+clusterCount);
            
      
            int maxValueInMap=(Collections.max(clusterMap.values()));  // This will return max value in the Hashmap
            for (Entry<Long, Integer> entry : clusterMap.entrySet())
            {  // Iterate through hashmap
            if (entry.getValue()==maxValueInMap) {
                System.out.println("ClusterID: "+entry.getKey());
                System.out.println("No of photoIDs: "+maxValueInMap+"\n");     // Print the key with max value
            }
        }
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(ClusterAlgorithmModified.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
