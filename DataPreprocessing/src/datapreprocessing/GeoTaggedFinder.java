/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataPreprocessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author KINCHIT RATHOD
 */
public class GeoTaggedFinder {

    private static final String REGEX = "(geo:lat=|geolat)";
    private static final String INPUT_FILE = "E:\\PlacingTaskData\\Demo\\Test.csv";
    private static final String LATLON_FILE = "E:\\PlacingTaskData\\Demo\\LatLongData";
    private static final String OUTPUT_FILE1 = "E:\\PlacingTaskData\\Demo\\TestGeoTaggedRefined.csv";
    private static final String OUTPUT_FILE2 = "E:\\PlacingTaskData\\Demo\\TestSortedGeoTags.csv";
    
    public static void main(String args[]) {

        try {
            ArrayList<DOGeoLocation> arrayList = new ArrayList<>();
            DOGeoLocation[] iDOGeoLocation = new DOGeoLocation[0];
            
            Pattern p = Pattern.compile(REGEX);
            BufferedReader lBufferedReader = new BufferedReader(new FileReader(INPUT_FILE));
            
            long count=0;
            String data=null;
            String line = null;
            while ((line = lBufferedReader.readLine()) != null) {
                Matcher m = p.matcher(line);
                while (m.find()) {
                    //System.out.println(line);
                    String[] infos = line.split(",");
                    String lineTrain = null;
                    //System.out.println(infos[0]);
                    
                    BufferedReader lBufferedTrain = new BufferedReader(new FileReader(LATLON_FILE));
                    BufferedWriter writer  = new BufferedWriter(new FileWriter(OUTPUT_FILE1,true));
                    
                    while ((lineTrain = lBufferedTrain.readLine()) != null) {
                        //System.out.println(lineTrain);
                        if (lineTrain.startsWith(infos[0])) {
                            //System.out.println(lineTrain);
                            String[] lineTrainSplit = lineTrain.split(" ");
                            DOGeoLocation lDOGeoLocation = new DOGeoLocation();
                            lDOGeoLocation.setLatitude(Double.parseDouble(lineTrainSplit[1]));
                            lDOGeoLocation.setLongitutde(Double.parseDouble(lineTrainSplit[2]));
                            lDOGeoLocation.setPhotoID(lineTrainSplit[0]);
                            arrayList.add(lDOGeoLocation);
                            
                            // Saving Merged data in Geotaggeddata.cscv
                            System.out.println(line + " ," + lineTrainSplit[1] + " ," + lineTrainSplit[2]);
                            writer.write(line + " ," + lineTrainSplit[1] + " ," + lineTrainSplit[2]+"\n");
                            writer.close();
                            count++;
                            break;
                        }
                       
                    }
                }
            }

           System.out.println("Total Mapped Records:"+count);
           
           BufferedWriter latlongwriter  = new BufferedWriter(new FileWriter(OUTPUT_FILE2,true));
           
            iDOGeoLocation =(DOGeoLocation[]) arrayList.toArray(iDOGeoLocation);
            Arrays.sort(iDOGeoLocation);
            for(DOGeoLocation aDOGeoLocation:iDOGeoLocation) 
            {
                //System.out.println(aDOGeoLocation.getPhotoID()+" "+aDOGeoLocation.getLatitude()+" "+aDOGeoLocation.getLongitutde());
                latlongwriter.write(aDOGeoLocation.getPhotoID()+" "+aDOGeoLocation.getLatitude()+" "+aDOGeoLocation.getLongitutde()+"\n");
            }
            
            latlongwriter.close();
   
            for(int i=0;i<2;i++)
                try {
                    Thread.sleep(500);
                      Other.Beep.makeasound();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeoTaggedFinder.class.getName()).log(Level.SEVERE, null, ex);
                }
        } catch (FileNotFoundException e) {

        } catch (IOException ex) {
            Logger.getLogger(GeoTaggedFinder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
