/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DataPreprocessing;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KINCHIT RATHOD
 */
public class SortGeoTagRefined {

    public static final String INPUT_FILE = "E:\\PlacingTaskData\\GeoTagRefined.csv";
    public static final String SORTED_GEOTAGREFINED="E:\\PlacingTaskData\\Demo\\SortedGeoTags.txt";
    
    public static void main(String args[]) 
    {

        BufferedReader iBufferedReader;
        try {
            
            ArrayList<DOGeoLocation> arrayList = new ArrayList<>();
            DOGeoLocation[] iDOGeoLocation = new DOGeoLocation[0];
            
            
            iBufferedReader = new BufferedReader(new FileReader(INPUT_FILE));
            String line = null;
            
            while ((line = iBufferedReader.readLine()) != null) {
                //System.out.println(line);
                String[] infos = line.split(",");
                //System.out.println(infos[0]);
                //System.out.println(infos[9]);
                //System.out.println(infos[10]);
                DOGeoLocation lDOGeoLocation = new DOGeoLocation();
                lDOGeoLocation.setPhotoID(infos[0]);
                lDOGeoLocation.setLatitude(Double.parseDouble(infos[9]));
                lDOGeoLocation.setLongitutde(Double.parseDouble(infos[10]));
                arrayList.add(lDOGeoLocation);
                
            }
            
            FileOutputStream outputData = new FileOutputStream(SORTED_GEOTAGREFINED,true);
            PrintStream outputStream  = new PrintStream(outputData);
            
            iDOGeoLocation =(DOGeoLocation[]) arrayList.toArray(iDOGeoLocation);
            long  count1=0;
            Arrays.sort(iDOGeoLocation);
            for(DOGeoLocation aDOGeoLocation:iDOGeoLocation) 
            {
                count1++;
                System.out.println(aDOGeoLocation.getPhotoID()+" "+aDOGeoLocation.getLatitude()+" "+aDOGeoLocation.getLongitutde());
                outputStream.println(aDOGeoLocation.getPhotoID()+" "+aDOGeoLocation.getLatitude()+" "+aDOGeoLocation.getLongitutde());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SortGeoTagRefined.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SortGeoTagRefined.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
