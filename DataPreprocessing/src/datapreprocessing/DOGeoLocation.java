/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataPreprocessing;

/**
 *
 * @author KINCHIT RATHOD
 */
public class DOGeoLocation implements Comparable {

    private String photoID;
    private int accuracy;
    private String userID;
    private String photoURL;
    private String tags;
    private String dateTaken;
    private String dateUploaded;
    private double latitude;
    private double longitutde;

    public String getPhotoID() {
        return photoID;
    }

    public void setPhotoID(String photoID) {
        this.photoID = photoID;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(String dateTaken) {
        this.dateTaken = dateTaken;
    }

    public String getDateUploaded() {
        return dateUploaded;
    }

    public void setDateUploaded(String dateUploaded) {
        this.dateUploaded = dateUploaded;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitutde() {
        return longitutde;
    }

    public void setLongitutde(double longitutde) {
        this.longitutde = longitutde;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof DOGeoLocation)) {
            throw new ClassCastException("Blah blah blah");
        }
        DOGeoLocation obj = (DOGeoLocation) o;
        double lat = obj.getLatitude();
        double lon = obj.getLongitutde();

        // distance function
        if (latitude < lat && longitutde < lon) {
            return 0;
        } else {
            return 1;
        }

    }

}
