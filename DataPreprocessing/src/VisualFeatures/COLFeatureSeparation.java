/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package VisualFeatures;

import DataPreprocessing.GeoTaggedFinder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KINCHIT RATHOD
 */
public class COLFeatureSeparation {
    
    private static final String INPUT_FILE="E:/PlacingTaskData/COLFeaturesRefined.txt";
    private static final String OUTPUT_FILE1="E:/PlacingTaskData/COLFeaturesRefinedSeparated1_First64Features.txt";
    private static final String OUTPUT_FILE2="E:/PlacingTaskData/COLFeaturesRefinedSeparated2_next28Features.txt";
    private static final String OUTPUT_FILE3="E:/PlacingTaskData/COLFeaturesRefinedSeparated3_next28Features.txt";
    
    public static void main(String args[]) throws FileNotFoundException, IOException
    {
        
        boolean success1 = (new File(OUTPUT_FILE1)).delete();
        if (success1) 
            System.out.println("COLFeaturesRefinedSeparated 1 Deleted!");
        boolean success2 = (new File(OUTPUT_FILE2)).delete();
        if(success2)
            System.out.println("COLFeaturesRefinedSeparated 2 Deleted!");
        boolean success3 = (new File(OUTPUT_FILE3)).delete();
        if(success3)
            System.out.println("COLFeaturesRefinedSeparated 3 Deleted!");
        
        BufferedReader featureReader  = new BufferedReader(new FileReader(INPUT_FILE));
        String lineStr = null;
        
        
        String imageID = null;
        
        FileOutputStream fileOutput64 = new FileOutputStream(OUTPUT_FILE1);
        PrintStream outputPrint64 = new PrintStream(fileOutput64);
        FileOutputStream fileOutput1_28 = new FileOutputStream(OUTPUT_FILE2);
        PrintStream outputPrint1_28 = new PrintStream(fileOutput1_28);
        FileOutputStream fileOutput2_28 = new FileOutputStream(OUTPUT_FILE3);
        PrintStream outputPrint2_28 = new PrintStream(fileOutput2_28);
        
        
        while((lineStr=featureReader.readLine())!=null)
        {
            
            StringBuilder featurespart1 = new StringBuilder();
            StringBuilder featurespart2 = new StringBuilder();
            StringBuilder featurespart3 = new StringBuilder();  

           imageID = lineStr.substring(0, lineStr.indexOf(" "));
//         System.out.println(imageID); 
           
           featurespart1.append(imageID+" ");
           featurespart1.append(lineStr.substring(lineStr.lastIndexOf("col") + 4, lineStr.indexOf('z')).replace("z", " "));
//         featurespart1=lineStr.substring(lineStr.lastIndexOf("col") + 4, lineStr.indexOf('z')).replace("z", " ");
//         System.out.println(featurespart1);
           outputPrint64.println(featurespart1);
           
           
           featurespart2.append(imageID+" ");
           featurespart2.append(lineStr.substring(lineStr.indexOf("z")+1, lineStr.lastIndexOf("z")).replace("z", " "));
           
//         featurespart2 =lineStr.substring(lineStr.indexOf("z")+1, lineStr.lastIndexOf("z")).replace("z", " ");
//         System.out.println(featurespart2);
           outputPrint1_28.println(featurespart2);
           
           
           featurespart3.append(imageID+" ");
           featurespart3.append(lineStr.substring(lineStr.lastIndexOf("z")+1, lineStr.length()).replace("z"," "));
//           featurespart3 = lineStr.substring(lineStr.lastIndexOf("z")+1, lineStr.length()).replace("z"," ");
//           System.out.println(featurespart3);
           outputPrint2_28.println(featurespart3);
           
//           break;        
        }
        
        for (int i = 0; i < 3; i++) {
                try {
                    Thread.sleep(300);
                    Other.Beep.makeasound();
                } catch (InterruptedException ex) {
                    System.out.println(ex.toString());
                }
        
        
    }
    }
}
