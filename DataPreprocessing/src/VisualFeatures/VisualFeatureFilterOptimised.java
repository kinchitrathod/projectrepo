/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VisualFeatures;

import DataPreprocessing.GeoTaggedFinder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author KINCHIT RATHOD
 * This file filters the imagefeatures files into separate features corresponding to their photoIDs
 */
public class VisualFeatureFilterOptimised extends Thread {

    public void run() {
        try {

            FileOutputStream fileOutputOPHIST = new FileOutputStream(OUTPUTFILE_OPHIST);
            PrintStream outputPrintOPHIST = new PrintStream(fileOutputOPHIST);
            FileOutputStream fileOutputGABOR = new FileOutputStream(OUTPUTFILE_GABOR);
            PrintStream outputPrintGABOR = new PrintStream(fileOutputGABOR);
            FileOutputStream fileOutputJHIST = new FileOutputStream(OUTPUTFILE_JHIST);
            PrintStream outputPrintJHIST = new PrintStream(fileOutputJHIST);
            FileOutputStream fileOutputJOPHIST = new FileOutputStream(OUTPUTFILE_JOPHIST);
            PrintStream outputPrintJOPHIST = new PrintStream(fileOutputJOPHIST);
            FileOutputStream fileOutputRGB = new FileOutputStream(OUTPUTFILE_RGB);
            PrintStream outputPrintRGB = new PrintStream(fileOutputRGB);

            String line = null;
            int count = 0, count1 = 0;
            try (BufferedReader IBufferedReader = new BufferedReader(new FileReader(INPUT_FILE))) {

                while ((line = IBufferedReader.readLine()) != null) {

                    StringBuilder ophist = new StringBuilder();
                    StringBuilder gabor = new StringBuilder();
                    StringBuilder jhist = new StringBuilder();
                    StringBuilder jophist = new StringBuilder();
                    StringBuilder rgb = new StringBuilder();

                    String photoID = line.substring(0, line.indexOf("acc")).trim();
                    //System.out.println("PhotoID:" + photoID);

                    rgb.append(line.substring((line.indexOf("RGB") - 1), line.indexOf("tamura")).trim());
                    outputPrintRGB.println(photoID + " " + rgb.toString());

                    ophist.append(line.substring((line.indexOf("ophist") - 1), line.indexOf("gabor")).trim());
                    outputPrintOPHIST.println(photoID + " " + ophist.toString());

                    gabor.append(line.substring((line.indexOf("gabor") - 1), line.indexOf("jhist")).trim());
                    outputPrintGABOR.println(photoID + " " + gabor.toString());

                    jhist.append(line.substring((line.indexOf("jhist") - 1), line.indexOf("jophist")).trim());
                    outputPrintJHIST.println(photoID + " " + jhist.toString());

                    jophist.append(line.substring((line.indexOf("jophist") - 1), line.indexOf("scalablecolor")).trim());
                    outputPrintJOPHIST.println(photoID + " " + jophist.toString());

                }
            }

        } catch (Exception e) {
        }
    }

    private static final String INPUT_FILE = "E:/imagefeatures/imagefeatures_6";
    private static final String OUTPUTFILE_ACC = "E:/PlacingTaskData/ImageFile6/imagefeatures_ACC_6.txt";
    private static final String OUTPUTFILE_BF = "E:/PlacingTaskData/ImageFile6/imagefeatures_BF_6.txt";
    private static final String OUTPUTFILE_CEDD = "E:/PlacingTaskData/ImageFile6/imagefeatures_CEDD_6.txt";
    private static final String OUTPUTFILE_COL = "E:/PlacingTaskData/ImageFile6/imagefeatures_COL_6.txt";
    private static final String OUTPUTFILE_EDGEHISTOGRAM = "E:/PlacingTaskData/ImageFile6/imagefeatures_EDGEHISTOGRAM_6.txt";
    private static final String OUTPUTFILE_FCTH = "E:/PlacingTaskData/ImageFile6/imagefeatures_FCTH_6.txt";
    private static final String OUTPUTFILE_OPHIST = "E:/PlacingTaskData/ImageFile6/imagefeatures_OPHIST_6.txt";
    private static final String OUTPUTFILE_GABOR = "E:/PlacingTaskData/ImageFile6/imagefeatures_GABOR_6.txt";
    private static final String OUTPUTFILE_JHIST = "E:/PlacingTaskData/ImageFile6/imagefeatures_JHIST_6.txt";
    private static final String OUTPUTFILE_JOPHIST = "E:/PlacingTaskData/ImageFile6/imagefeatures_JOPHIST_6.txt";
    private static final String OUTPUTFILE_SCALABLECOLOR = "E:/PlacingTaskData/ImageFile6/imagefeatures_SCALABLECOLOR_6.txt";
    private static final String OUTPUTFILE_RGB = "E:/PlacingTaskData/ImageFile6/imagefeatures__RGB_6.txt";
    private static final String OUTPUTFILE_TAMURA = "E:/PlacingTaskData/ImageFile6/imagefeatures_TAMURA_6.txt";

    public static void main(String args[]) {

        VisualFeatureFilterOptimised FilterThread = new VisualFeatureFilterOptimised();
        FilterThread.start();

        try {

            FileOutputStream fileOutputACC = new FileOutputStream(OUTPUTFILE_ACC);
            PrintStream outputPrintACC = new PrintStream(fileOutputACC);
            FileOutputStream fileOutputBF = new FileOutputStream(OUTPUTFILE_BF);
            PrintStream outputPrintBF = new PrintStream(fileOutputBF);
            FileOutputStream fileOutputCEDD = new FileOutputStream(OUTPUTFILE_CEDD);
            PrintStream outputPrintCEDD = new PrintStream(fileOutputCEDD);
            FileOutputStream fileOutputCOL = new FileOutputStream(OUTPUTFILE_COL);
            PrintStream outputPrintCOL = new PrintStream(fileOutputCOL);
            FileOutputStream fileOutputEHD = new FileOutputStream(OUTPUTFILE_EDGEHISTOGRAM);
            PrintStream outputPrintEHD = new PrintStream(fileOutputEHD);
            FileOutputStream fileOutputFCTH = new FileOutputStream(OUTPUTFILE_FCTH);
            PrintStream outputPrintFCTH = new PrintStream(fileOutputFCTH);
            FileOutputStream fileOutputSCD = new FileOutputStream(OUTPUTFILE_SCALABLECOLOR);
            PrintStream outputPrintSCD = new PrintStream(fileOutputSCD);
            FileOutputStream fileOutputTAMURA = new FileOutputStream(OUTPUTFILE_TAMURA);
            PrintStream outputPrintTAMURA = new PrintStream(fileOutputTAMURA);

            String line = null;
            int count = 0, count1 = 0;
            try (BufferedReader IBufferedReader = new BufferedReader(new FileReader(INPUT_FILE))) {

                while ((line = IBufferedReader.readLine()) != null) {
                    StringBuilder acc = new StringBuilder();
                    StringBuilder bf = new StringBuilder();
                    StringBuilder cedd = new StringBuilder();
                    StringBuilder cld = new StringBuilder();
                    StringBuilder ehd = new StringBuilder();
                    StringBuilder fcth = new StringBuilder();

                    StringBuilder scd = new StringBuilder();

                    StringBuilder tamura = new StringBuilder();

                    String photoID = line.substring(0, line.indexOf("acc")).trim();
                    System.out.println("PhotoID:" + photoID);

                    acc.append(line.substring((line.indexOf("acc") - 1), line.indexOf("bf")).trim());
                    outputPrintACC.println(photoID + " " + acc.toString());

                    bf.append(line.substring((line.indexOf("bf") - 1), line.indexOf("cedd")).trim());
                    outputPrintBF.println(photoID + " " + bf.toString());

                    cedd.append(line.substring((line.indexOf("cedd") - 1), line.indexOf("col")).trim());
                    outputPrintCEDD.println(photoID + " " + cedd.toString());

                    cld.append(line.substring((line.indexOf("col") - 1), line.indexOf("edgehistogram")).trim());
                    outputPrintCOL.println(photoID + " " + cld.toString());

                    ehd.append(line.substring((line.indexOf("edgehistogram") - 1), line.indexOf("fcth")).trim());
                    outputPrintEHD.println(photoID + " " + ehd.toString());

                    fcth.append(line.substring((line.indexOf("fcth") - 1), line.indexOf("ophist")).trim());
                    outputPrintFCTH.println(photoID + " " + fcth.toString());

                    scd.append(line.substring((line.indexOf("scalablecolor") - 1), line.indexOf("RGB")).trim());
                    outputPrintSCD.println(photoID + " " + scd.toString());

                    tamura.append(line.substring((line.indexOf("tamura") - 1), line.length()).trim());
                    outputPrintTAMURA.println(photoID + " " + tamura.toString());

                }
            }
            for (int i = 0; i < 3; i++) {
                try {
                    Thread.sleep(300);
                    Other.Beep.makeasound();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeoTaggedFinder.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

         } catch (FileNotFoundException e) 
        {
                System.out.println(e.toString());
        }
        catch (IOException ex) {
            System.out.println(ex.toString());
        }
    }
}
