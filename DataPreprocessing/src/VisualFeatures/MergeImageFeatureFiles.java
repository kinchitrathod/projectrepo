/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package VisualFeatures;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;

/**
 *
 * @author KINCHIT RATHOD
 */
public class MergeImageFeatureFiles {
    
    private static final String INPUT_FILE1 = "E:\\PlacingTaskData\\ImageFile6\\ImageFeatureRemainingEHDExtracted.txt";
    private static final String OUTPUT_FILE = "E:/PlacingTaskData/EHDFeaturesRefined.txt";
    
   public static void main(String[] args)
   {
       try {
           
           BufferedReader ImageFeatureReader = new BufferedReader(new FileReader(INPUT_FILE1));
           String strFeatures = null;
            FileOutputStream fileOutputFeatures = new FileOutputStream(OUTPUT_FILE,true);
            PrintStream out = new PrintStream(fileOutputFeatures);
           
           while((strFeatures = ImageFeatureReader.readLine())!=null)
           {
               out.println(strFeatures);
           }
           
           Other.Beep.makeasound();
       
       } catch (Exception e) {
           e.printStackTrace();
       }
   }
}
