/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package VisualFeatures;

import DataPreprocessing.GeoTaggedFinder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KINCHIT RATHOD
 */
public class COLEHDFeatureMerging {
    
    private static final String INPUT_FILE1 = "E:/PlacingTaskData/COLFeaturesRefinedSeparated1_First64Features.txt";
    private static final String INPUT_FILE2 = "E:/PlacingTaskData/COLFeaturesRefinedSeparated2_next28Features.txt";
    private static final String INPUT_FILE3 = "E:/PlacingTaskData/COLFeaturesRefinedSeparated3_next28Features.txt";
    private static final String INPUT_FILE4 = "E:/PlacingTaskData/EHDFeaturesRefined.txt";
    private static final String OUTPUT_FILE1 = "E:/PlacingTaskData/COLFeatures_12Features.txt";
    private static final String OUTPUT_FILE2 = "E:/PlacingTaskData/COLEHDFeaturesMerged_92Features.txt";
    
    public static void main(String args[])
    {
        
        boolean success1 = (new File(OUTPUT_FILE1)).delete();
        if (success1) 
            System.out.println("COLFeatures_12Features Deleted!");
        
        boolean success2 = (new File(OUTPUT_FILE2)).delete();
        if (success2) 
            System.out.println("COLEHDFeaturesMerged_92Features Deleted!");
        
        try {
            
            
            FileOutputStream fileOutput12 = new FileOutputStream(OUTPUT_FILE1);
            PrintStream outputPrint12 = new PrintStream(fileOutput12);
            FileOutputStream fileOutput92 = new FileOutputStream(OUTPUT_FILE2);
            PrintStream outputPrint92 = new PrintStream(fileOutput92);
            BufferedReader fileReader=null;
            String lineStr=null;
            String features=null;
            String imageID=null;
            
            HashMap<String,String> featureMap = new LinkedHashMap<>();     
            
            fileReader = new BufferedReader(new FileReader(INPUT_FILE1));
            
            while((lineStr=fileReader.readLine())!=null)
            {
                StringBuilder mergeFeatures = new StringBuilder();
                String featureSplit[]=null;
                String lineSplit[] = null;
                featureSplit = lineStr.split(",");
//                System.out.println(lineStr);
                imageID= lineStr.substring(0, lineStr.indexOf(" "));
                features= lineStr.substring(lineStr.indexOf(" "),lineStr.length());
                featureSplit = features.split(" ");
                
                for(int i=1;i<=6;i++)
                {
                    mergeFeatures.append(featureSplit[i]);
                    mergeFeatures.append(" ");
                }
//                System.out.println(mergeFeatures);
                
                featureMap.put(imageID, mergeFeatures.toString());
//              break;  
            }
                    
            
//            for (String key : featureMap.keySet()) {
//                
//                System.out.println(featureMap.get(key)+" "+key);
//            }
           
            fileReader.close();
              
            fileReader = new BufferedReader(new FileReader(INPUT_FILE2));
             while((lineStr=fileReader.readLine())!=null)
            {
                StringBuilder mergeFeatures = new StringBuilder();
                String featureSplit[]=null;
                String lineSplit[] = null;
                StringBuilder str = new StringBuilder();
                featureSplit = lineStr.split(",");
                //System.out.println(lineStr);
                imageID= lineStr.substring(0, lineStr.indexOf(" "));
                features= lineStr.substring(lineStr.indexOf(" "),lineStr.length());
                featureSplit = features.split(" ");
                
                for(int i=1;i<=3;i++)
                {
                    str.append(featureSplit[i]);
                    str.append(" ");
                }
 //               System.out.println(str);
                mergeFeatures.append(featureMap.get(imageID));
                mergeFeatures.append(str);
//                System.out.println(mergeFeatures);
                featureMap.put(imageID, mergeFeatures.toString());
            }
        
//            for (String key : featureMap.keySet()) {
//                
//                System.out.println(featureMap.get(key)+" "+key);
//            }
            
            fileReader.close();
            
            fileReader = new BufferedReader(new FileReader(INPUT_FILE3));
            
             while((lineStr=fileReader.readLine())!=null)
            {
                StringBuilder mergeFeatures = new StringBuilder();
                String featureSplit[]=null;
                String lineSplit[] = null;
                StringBuilder str = new StringBuilder();
                featureSplit = lineStr.split(",");
                //System.out.println(lineStr);
                imageID= lineStr.substring(0, lineStr.indexOf(" "));
                features= lineStr.substring(lineStr.indexOf(" "),lineStr.length());
                featureSplit = features.split(" ");
                
                for(int i=1;i<=3;i++)
                {
                    str.append(featureSplit[i]);
                    str.append(" ");
                }
//                System.out.println(str);
                
                mergeFeatures.append(featureMap.get(imageID));
                mergeFeatures.append(str);
//                System.out.println(mergeFeatures);
                
                featureMap.put(imageID, mergeFeatures.toString());
                
//              break;  
            }
        
            for (String key : featureMap.keySet()) {
                
                //System.out.println(key+" "+featureMap.get(key));
                outputPrint12.println(key+" "+featureMap.get(key));
            }
            
            fileReader.close();
            
            fileReader = new BufferedReader(new FileReader(INPUT_FILE4));
            
             while((lineStr=fileReader.readLine())!=null)
            {
                StringBuilder mergeFeatures = new StringBuilder();
                String featureSplit[]=null;
                String lineSplit[] = null;
                StringBuilder str = new StringBuilder();
                featureSplit = lineStr.split(",");
                //System.out.println(lineStr);
                imageID= lineStr.substring(0, lineStr.indexOf(" "));
                features= lineStr.substring(lineStr.lastIndexOf("edgehistogram")+13,lineStr.length());
                                
                str.append(features);
                               
                mergeFeatures.append(featureMap.get(imageID).trim());
                mergeFeatures.append(str);
                //System.out.println(mergeFeatures);
                
                featureMap.put(imageID, mergeFeatures.toString());
                
//              break;  
            }
            
            for (String key : featureMap.keySet()) {
                //System.out.println(key+" "+featureMap.get(key));
                outputPrint92.println(key+" "+featureMap.get(key));
            }
            
            

        } catch (FileNotFoundException ex) {
            Logger.getLogger(COLEHDFeatureMerging.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(COLEHDFeatureMerging.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for (int i = 0; i < 3; i++) {
                try {
                     Thread.sleep(300);
                    Other.Beep.makeasound();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeoTaggedFinder.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        
        
    }
  
    
}
