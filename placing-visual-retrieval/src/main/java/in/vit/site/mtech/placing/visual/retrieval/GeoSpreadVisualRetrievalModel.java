/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.vit.site.mtech.placing.visual.retrieval;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import uk.qmul.mmv.dataobjects.DOPresentation;
import uk.qmul.mmv.queryclassifier.PSOClassifier;
import uk.qmul.mmv.utils.InitialParameter;
import uk.qmul.mmv.utils.SpecialFeature;

public class GeoSpreadVisualRetrievalModel {
    
    private static final String INPUT_FEATUREFILE = "E:/PlacingTaskData/COLFeaturesRefinedNew.txt";
    private static final String INPUT_CLUSTERFILE = "E:/PlacingTaskData/ClusterFiles/ClusterImagesOnly.txt";
    private static final String OUTPUT_RETRIEVALMODEL = "E:/PlacingTaskData/RetrievalModelLessFeatures1.txt";

    public static void main(String[] agrgs) {

        try {
            List<DOPresentation> arrayPresentation = new ArrayList<>();
            DOPresentation[] testFeatures = new DOPresentation[0];
            String readClusterQuery = null;
            double[] queryFeatures = null;
            BufferedReader featuresQueryTest = new BufferedReader(new FileReader(INPUT_FEATUREFILE));
            String readFeatureQueryTest = null;

            while ((readFeatureQueryTest = featuresQueryTest.readLine()) != null) {
                readFeatureQueryTest = readFeatureQueryTest.substring(readFeatureQueryTest.lastIndexOf("col") + 4, readFeatureQueryTest.length()).replace("z", " ").trim();
                queryFeatures = SpecialFeature.convertStringDouble(readFeatureQueryTest, Integer.parseInt(InitialParameter.getValue("placing.cld.dim")) + 1);
                DOPresentation iDOPresentation = new DOPresentation();
                iDOPresentation.setFeatures(queryFeatures);
                iDOPresentation.setPhotoID(readFeatureQueryTest.substring(0, readFeatureQueryTest.indexOf(" ")));
                arrayPresentation.add(iDOPresentation);
                //System.out.println("row added");
                //System.out.println(arrayPresentation.size());
            }
            
            testFeatures = (DOPresentation[]) arrayPresentation.toArray(testFeatures);
            //System.out.println("testFeatures.length:" + testFeatures.length);

            BufferedReader clusterReaderQuery = new BufferedReader(new FileReader(INPUT_CLUSTERFILE));

            PSOClassifier lPSOClassifier = new PSOClassifier();

            FileOutputStream outputFile = new FileOutputStream(OUTPUT_RETRIEVALMODEL);
            PrintStream printOutputFile = new PrintStream(outputFile);

            while ((readClusterQuery = clusterReaderQuery.readLine()) != null) {

                String clusterID = readClusterQuery.substring(0, readClusterQuery.indexOf(" "));
                System.out.println("ClusterID:"+clusterID);
                String[] imageIDSplits = readClusterQuery.substring(readClusterQuery.indexOf(" ") + 1).split(",");
                for (String imageIDSplit : imageIDSplits) {
                    
                    //System.out.println("ImageIDSplit:"+imageIDSplit);
                    
                    String imageID = imageIDSplit.substring(0, imageIDSplit.indexOf(" "));
                    System.out.println("ImageID:"+imageID);
                    
                    BufferedReader featuresQuery = new BufferedReader(new FileReader(INPUT_FEATUREFILE));
                    String readFeatureQuery = null;
                    while ((readFeatureQuery = featuresQuery.readLine()) != null) {

                        if (readFeatureQuery.startsWith(imageID)) {
                            // eliminate the image id and feature descriptor from readFeatureQuery
                            readFeatureQuery = readClusterQuery.substring(readFeatureQuery.lastIndexOf("col") + 4, readFeatureQuery.indexOf('z')).replace("z", " ");
                            //readFeatureQuery = readFeatureQuery.substring(readFeatureQuery.lastIndexOf("col") + 4, readFeatureQuery.length()).replace("z", " ").trim();
                            //System.out.println(readFeatureQuery);
                            queryFeatures = SpecialFeature.convertStringDouble(readFeatureQuery, Integer.parseInt(InitialParameter.getValue("placing.cld.dim")));
                            break;
                        }
                    }

                    //System.out.println("queryFeatures.length: "+queryFeatures.length);
                    lPSOClassifier.trainDown(queryFeatures);
                    lPSOClassifier.trainUp(queryFeatures);

                    DOPresentation[] lDOPresentation = lPSOClassifier.classify(testFeatures, imageIDSplit, Integer.parseInt(clusterID));
                    Arrays.sort(lDOPresentation);

                    // output format for the file
                    // image ID - similar images
                    printOutputFile.print(imageIDSplit + ":");
                    for (int i = 0; i < 100; i++) {
                        printOutputFile.print(lDOPresentation[i].getPhotoID() + ",");
                    }
                    printOutputFile.println();
                }
            }

            //DOPresentation[] testFeatures=lDACorelDatabase.extractFeatures();
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace(System.out);
        }

    }
}
