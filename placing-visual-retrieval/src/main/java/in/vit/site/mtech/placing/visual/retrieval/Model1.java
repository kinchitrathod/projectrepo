/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.vit.site.mtech.placing.visual.retrieval;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import uk.qmul.mmv.dataobjects.DOPresentation;
import uk.qmul.mmv.queryclassifier.PSOClassifier;
import uk.qmul.mmv.utils.InitialParameter;
import uk.qmul.mmv.utils.SpecialFeature;

public class Model1 {

    public static void main(String[] agrgs) {

        try {
            List<DOPresentation> arrayPresentation = new ArrayList<>();
            DOPresentation[] testFeatures = new DOPresentation[0];
            String readClusterQuery = null;
            double[] queryFeatures = null;
            BufferedReader featuresQueryTest = new BufferedReader(new FileReader("E:/PlacingTaskData/COLFeaturesRefined.txt"));
            String readFeatureTest = null;

            while ((readFeatureTest = featuresQueryTest.readLine()) != null) {
                String readFeatureQueryTest = readFeatureTest.substring(readFeatureTest.lastIndexOf("col") + 4, readFeatureTest.indexOf('z')).replace("z", " ");
                queryFeatures = SpecialFeature.convertStringDouble(readFeatureQueryTest, Integer.parseInt(InitialParameter.getValue("placing.cld.dim")) + 1);
                DOPresentation iDOPresentation = new DOPresentation();
                iDOPresentation.setFeatures(queryFeatures);
                iDOPresentation.setPhotoID(readFeatureTest.substring(0, readFeatureTest.indexOf(" ")));
                //System.out.println(iDOPresentation.getPhotoID());
                arrayPresentation.add(iDOPresentation);
                //System.out.println("row added");
                //System.out.println(arrayPresentation.size());
                //System.out.println("QueryFeatures.length:" + queryFeatures.length); // Length: 121
                
            }
            
            
            
            testFeatures = (DOPresentation[]) arrayPresentation.toArray(testFeatures);
            
           // for(int k=0; k<testFeatures.length; k++) {
                //DOPresentation testFeature=testFeatures[k];
                //System.out.println(testFeature.getClusterID()+"-"+testFeature.getPhotoID());
            //}
            
            //System.out.println("testFeatures.length:" + testFeatures.length);
            featuresQueryTest.close();
            
            BufferedReader clusterReaderQuery = new BufferedReader(new FileReader("E:/PlacingTaskData/ClusterFiles/Clusters.txt"));

            FileOutputStream outputFile = new FileOutputStream("E:/PlacingTaskData/RetrievalModelLessFeatures1.txt");
            PrintStream printOutputFile = new PrintStream(outputFile);

            while ((readClusterQuery = clusterReaderQuery.readLine()) != null) {
                //System.out.println("Read Cluster Line");
                String clusterID = readClusterQuery.substring(0, readClusterQuery.indexOf(" "));
                System.out.println("ClusterID:"+clusterID);
                String[] imageIDSplits = readClusterQuery.substring(readClusterQuery.indexOf(" ") + 1).split(",");
                for (String imageIDSplit : imageIDSplits) {
                    String imageID = imageIDSplit.substring(0, imageIDSplit.indexOf(" "));
                   
                    BufferedReader featuresQuery = new BufferedReader(new FileReader("E:\\PlacingTaskData\\ImageFeaturesMerged\\imagefeatures_COL.txt"));
                    String readFeatureQuery = null;
                    String readFeature = null;
                    while ((readFeatureQuery = featuresQuery.readLine()) != null) 
                    {
                        //System.out.println(imageID);
                        if (readFeatureQuery.startsWith(imageID)){ 
                            //System.out.println("1");
                            // eliminate the image id and feature descriptor from readFeatureQuery
                            readFeature = readFeatureQuery.substring(readFeatureQuery.lastIndexOf("col") + 4, readFeatureQuery.indexOf('z')).replace("z", " ");
                            //System.out.println(readFeatureQuery);
                            queryFeatures = SpecialFeature.convertStringDouble(readFeature, Integer.parseInt(InitialParameter.getValue("placing.cld.dim")));
                            break;
                        }
                        
                    }

                    PSOClassifier lPSOClassifier = new PSOClassifier();
                    
                    //System.out.println("queryFeatures.length: "+queryFeatures.length);
                    lPSOClassifier.trainDown(queryFeatures);
                    lPSOClassifier.trainUp(queryFeatures);
                    
                    //System.out.println("2");
                    //System.out.println(testFeatures.length);
                    System.out.println(imageIDSplit);
                    //System.out.println(clusterID);
                    
                    DOPresentation[] lDOPresentation = lPSOClassifier.classify(testFeatures, imageIDSplit, Integer.parseInt(clusterID));
                   // Arrays.sort(lDOPresentation);
                    
                    //System.out.println("3");
                    // output format for the file
                    // image ID - similar images
                    
                    printOutputFile.print(imageIDSplit + ":");
                    System.out.println(imageIDSplit + ":");
                    for (int i = 0; i < 100; i++) {
                        printOutputFile.print(lDOPresentation[i].getPhotoID() + ",");
                        System.out.println(lDOPresentation[i].getPhotoID());
                    }
                    printOutputFile.println();
                    
                }
            }

            //DOPresentation[] testFeatures=lDACorelDatabase.extractFeatures();
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace(System.out);
        }

    }
}
