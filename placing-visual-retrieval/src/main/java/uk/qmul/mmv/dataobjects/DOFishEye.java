package uk.qmul.mmv.dataobjects;

public class DOFishEye {

	private DOImageInfo[] iDOImageinfo=null;

	public DOImageInfo[] getIDOImageinfo() {
		return iDOImageinfo;
	}

	public void setIDOImageinfo(DOImageInfo[] imageinfo) {
		iDOImageinfo = imageinfo;
	}
}
