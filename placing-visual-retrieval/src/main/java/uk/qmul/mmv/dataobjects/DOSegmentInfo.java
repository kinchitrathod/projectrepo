/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.qmul.mmv.dataobjects;

/**
 *
 * @author krishna
 */
public class DOSegmentInfo {

    private String segmentID=null;
    private String grammar=null;
    private int duration=0;

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setGrammar(String grammar) {
        this.grammar = grammar;
    }

    public void setSegmentID(String segmentID) {
        this.segmentID = segmentID;
    }

    public String getGrammar() {
        return grammar;
    }

    public String getSegmentID() {
        return segmentID;
    }
    
    
}
