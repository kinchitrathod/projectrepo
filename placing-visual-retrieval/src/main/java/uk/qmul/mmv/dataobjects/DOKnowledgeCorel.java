/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.qmul.mmv.dataobjects;

/**
 *
 * @author krishna
 */
public class DOKnowledgeCorel implements Comparable {

    private String idStillReg;
    private String mediaURI=null;
    private String concept=null;
    private String width=null;
    private String height=null;
    private int blockCount;
    private int scale;
    private DOPresentation[] lDOPresentation;

    public int getScale() {
        return scale;
    }

    public DOPresentation[] getLDOPresentation() {
        return lDOPresentation;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public void setLDOPresentation(DOPresentation[] lDOPresentation) {
        this.lDOPresentation = lDOPresentation;
    }

    public int compareTo(Object doPresentation) throws ClassCastException {
        if (!(doPresentation instanceof DOKnowledgeCorel))
            throw new ClassCastException("A DOKnowledgeCorel object expected.");
        int blockCountInternal = ((DOKnowledgeCorel) doPresentation).getBlockCount();  
        if(blockCountInternal>this.blockCount) {
            return 1;
        } else {
            return 0;
        }
    }

    public int getBlockCount() {
        return blockCount;
    }

    public String getConcept() {
        return concept;
    }

    public String getHeight() {
        return height;
    }

    public void setBlockCount(int blockCount) {
        this.blockCount = blockCount;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public void setIdStillReg(String idStillReg) {
        this.idStillReg = idStillReg;
    }

    public void setMediaURI(String mediaURI) {
        this.mediaURI = mediaURI;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getIdStillReg() {
        return idStillReg;
    }

    public String getMediaURI() {
        return mediaURI;
    }

    public String getWidth() {
        return width;
    }

    
    
}
