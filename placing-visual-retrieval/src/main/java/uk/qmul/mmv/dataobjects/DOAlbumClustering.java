/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.qmul.mmv.dataobjects;

/**
 *
 * @author krishna
 */
public class DOAlbumClustering {

    private DOClusterRepresentation[] lDOClusterRepresentation=null;
    private int clusterID=0;

    public int getClusterID() {
        return clusterID;
    }

    public void setClusterID(int clusterID) {
        this.clusterID = clusterID;
    }

    public DOClusterRepresentation[] getClusterRepresentation() {
        return lDOClusterRepresentation;
    }

    public void setClusterRepresentation(DOClusterRepresentation[] lDOClusterRepresentation) {
        this.lDOClusterRepresentation = lDOClusterRepresentation;
    }
}
