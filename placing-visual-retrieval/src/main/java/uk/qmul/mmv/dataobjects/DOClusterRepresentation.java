/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.qmul.mmv.dataobjects;

/**
 *
 * @author krishna
 */
public class DOClusterRepresentation {

    private int clusterID;
    private int imageID;
    private int width;
    private int height;

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    private String url=null;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getClusterID() {
        return clusterID;
    }

    public int getImageID() {
        return imageID;
    }

    public void setClusterID(int clusterID) {
        this.clusterID = clusterID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    
}
