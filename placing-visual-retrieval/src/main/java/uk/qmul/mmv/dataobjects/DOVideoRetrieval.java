/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.qmul.mmv.dataobjects;

/**
 *
 * @author krishna
 */
public class DOVideoRetrieval {

    private String idSegment=null;
    private int count=0;
    private String[] kfUri=null;
    private DOSegmentInfo[] segmentInfo;

    public void setSegmentInfo(DOSegmentInfo[] segmentInfo) {
        this.segmentInfo = segmentInfo;
    }

    public DOSegmentInfo[] getSegmentInfo() {
        return segmentInfo;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }
    private String mediaLoc=null;
    private int scale=0;

    public int getScale() {
        return scale;
    }

    public String getIdSegment() {
        return idSegment;
    }

    public void setMediaLoc(String mediaLoc) {
        this.mediaLoc = mediaLoc;
    }

    public String getMediaLoc() {
        return mediaLoc;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String[] getKfUri() {
        return kfUri;
    }

    public void setIdSegment(String idSegment) {
        this.idSegment = idSegment;
    }

    public void setKfUri(String[] kfUri) {
        this.kfUri = kfUri;
    }
}
