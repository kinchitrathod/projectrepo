package uk.qmul.mmv.dataobjects;

public class DOPresentation implements Comparable {

    private String photoID=null;
    private long clusterID=0;
    private double geoSpread=0.0;
    private double[] features = null;


    public String getPhotoID() {
        return photoID;
    }

    public void setPhotoID(String photoID) {
        this.photoID = photoID;
    }

    public long getClusterID() {
        return clusterID;
    }

    public void setClusterID(long clusterID) {
        this.clusterID = clusterID;
    }

    public double getGeoSpread() {
        return geoSpread;
    }

    public void setGeoSpread(double geoSpread) {
        this.geoSpread = geoSpread;
    }



    public void setScale(int scale) {
        this.scale = scale;
    }
    //private double retrievalWeight;

    public int getScale() {
        return scale;
    }
    private int scale = 1;

    public int compareTo(Object doPresentation) throws ClassCastException {
        if (!(doPresentation instanceof DOPresentation)) {
            throw new ClassCastException("A DOPresentation object expected.");
        }
        double anotherGeoSpread = ((DOPresentation) doPresentation).getGeoSpread();
        if (anotherGeoSpread < this.geoSpread) {
            return 1;
        } else {
            return 0;
        }
    }

    public void setFeatures(double[] features) {
        this.features = features;
    }

    public double[] getFeatures() {
        return features;
    }
}
