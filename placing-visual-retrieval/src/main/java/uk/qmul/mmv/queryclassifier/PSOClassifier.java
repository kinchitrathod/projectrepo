package uk.qmul.mmv.queryclassifier;

import java.util.ArrayList;

import uk.qmul.mmv.dataobjects.DOPresentation;
import uk.qmul.mmv.utils.InitialParameter;

public class PSOClassifier {

    private SelfOrganisingMaps somDown;
    private SelfOrganisingMaps somUp;
    private PsoOptimiser pso;

    private int rowCnt = 0;
    private double omegaDown = 1.0;
    private double omegaUp = 1.0;
    private final double th = 50.0;
    private double temp[];

    private int rows;
    private int cols;
    private int dim;
    private int i;
    private int j;

    public PSOClassifier() {
        rows = Integer.parseInt(InitialParameter.getValue("sofm.query.rows"));
        cols = Integer.parseInt(InitialParameter.getValue("sofm.query.cols"));

        dim = Integer.parseInt(InitialParameter.getValue("placing.cld.dim"));

        i = Integer.parseInt(InitialParameter.getValue("sofm.query.rows")) / 2;
        j = Integer.parseInt(InitialParameter.getValue("sofm.query.cols")) / 2;

        temp = new double[dim];
        somDown = new SelfOrganisingMaps("placing.cld");
        somUp = new SelfOrganisingMaps("placing.cld");
        //System.out.println("TEMP LENGTH: "+temp.length);
    }

    public void trainDown(double[] queryFeature) {
        for (rowCnt = 0; rowCnt <= rows / 2; rowCnt++) {
            for (i = ((rows / 2) - rowCnt); i <= ((rows / 2) + rowCnt); i++) {
                for (j = ((cols / 2) - rowCnt); j <= ((cols / 2) + rowCnt); j++) {
                    for (int k = 0; k < dim; k++) {
                        somDown.inp[k] = queryFeature[k] * omegaDown;
                        temp[k] = somDown.inpWt[i][j][k];
                    }
                    if (somDown.trainedNode[i][j] != 1 && somDown.trainedNode[i][j] != 2) {
                        PsoOptimiser pso = new PsoOptimiser("placing.cld", temp, somDown.inp);
                        pso.updateNeuronCld(th);
                        pso.personalBestCld();
                        pso.globalBestCld();
//                        System.out.println("Dim:" + dim);
                        for (int k = 0; k < dim; k++) {
                            somDown.inpWt[i][j][k] = pso.gbest[k];
                        }
                        if (rowCnt <= 1) {
                            somDown.trainedNode[i][j] = 1;
                        } else {
                            somDown.trainedNode[i][j] = 2;
                        }
                    }
                }
            }
            omegaDown = omegaDown - 0.167;
        }
        //System.out.println("Completed Training Successfully");
    }

    public void trainUp(double[] queryFeature) {
        for (rowCnt = 0; rowCnt <= rows / 2; rowCnt++) {
            for (i = ((rows / 2) - rowCnt); i <= ((rows / 2) + rowCnt); i++) {
                for (j = ((cols / 2) - rowCnt); j <= ((cols / 2) + rowCnt); j++) {
                    for (int k = 0; k < dim; k++) {
                        somUp.inp[k] = queryFeature[k] * omegaUp;
                        temp[k] = somUp.inpWt[i][j][k];
                    }
                    if (somUp.trainedNode[i][j] != 1 && somUp.trainedNode[i][j] != 2) {
                        PsoOptimiser pso = new PsoOptimiser("placing.cld", temp, somUp.inp);
                        pso.updateNeuronCld(th);
                        pso.personalBestCld();
                        pso.globalBestCld();
                        for (int k = 0; k < dim; k++) {
                            somUp.inpWt[i][j][k] = pso.gbest[k];
                        }
                        if (rowCnt <= 1) {
                            somUp.trainedNode[i][j] = 1;
                        } else {
                            somUp.trainedNode[i][j] = 2;
                        }
                    }
                }
            }
            omegaUp = omegaUp + 0.168;
        }
        //System.out.println("Completed Training Successfully");
    }

    //public DOQueryClassifier classify(double[] testFeature, String idStillReg) {
    //DOQueryClassifier lDOQueryClassifier=new DOQueryClassifier();
    //for(int k=0; k<dim; k++) {
    //somDown.inp[k]=testFeature[k];
    //somUp.inp[k]=testFeature[k];
    //}
    //somDown.computeTest();
    //somUp.computeTest();
    //if((somDown.trainedNode[somDown.winNode0][somDown.winNode1]==1) && (somUp.trainedNode[somUp.winNode0][somUp.winNode1]==1)) {
    //lDOQueryClassifier.setIdStillReg(idStillReg);
    //lDOQueryClassifier.setDistance(((somDown.winNodeValue+somUp.winNodeValue)/2));
    //lDOQueryClassifier.setDistance(somDown.winNodeValue);
    //} else {
    //lDOQueryClassifier.setIdStillReg("0");
    //lDOQueryClassifier.setDistance(500.0);
    //}
    //return lDOQueryClassifier;
    //}

    public DOPresentation[] classify(DOPresentation[] aDOPresentation, String database, int clusterID) {
        DOPresentation[] lDOPresentation = new DOPresentation[0];
        ArrayList<DOPresentation> aArrayList = new ArrayList<>();

        for (int i = 0; i < aDOPresentation.length; i++) {

            DOPresentation iDOPresentation = aDOPresentation[i];
            if (!iDOPresentation.getPhotoID().startsWith(database)) { // && iDOPresentation.getClusterID()!=clusterID) {
                //System.out.println("I am inside the if condition"+iDOPresentation.getPhotoID());
                
                double[] cld = (double[]) iDOPresentation.getFeatures();
                for (int k = 0; k < dim; k++) {
                    somDown.inp[k] = cld[k];
                    somUp.inp[k] = cld[k];
                }
                somDown.computeTest();
                somUp.computeTest();
                if ((somDown.trainedNode[somDown.winNode0][somDown.winNode1] == 1) && (somUp.trainedNode[somUp.winNode0][somUp.winNode1] == 1)) {
                    //iDOQueryClassifier.setDistance(((somDown.winNodeValue+somUp.winNodeValue)/2));
                    iDOPresentation.setGeoSpread(((somDown.winNodeValue + somUp.winNodeValue) / 2));
                } else {
                    iDOPresentation.setGeoSpread(50.0 + ((somDown.winNodeValue + somUp.winNodeValue) / 2));
                }
                //iDOPresentation.setScale(Integer.parseInt(InitialParameter.getValue(database+".scale")));
                aArrayList.add(iDOPresentation);

            }
        }
        lDOPresentation = (DOPresentation[]) aArrayList.toArray(lDOPresentation);
        return lDOPresentation;
    }
}
