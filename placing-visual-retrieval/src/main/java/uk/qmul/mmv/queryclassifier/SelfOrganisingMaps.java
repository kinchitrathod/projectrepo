package uk.qmul.mmv.queryclassifier;

import java.util.Random;
import java.lang.Math;

import uk.qmul.mmv.utils.InitialParameter;

public class SelfOrganisingMaps {

    public double inpWt[][][];
    private double distance[][];
    public double winNodeValue;
    private double temp, temp1, sum, sum1;
    public double inp[];
    public int winNode0, winNode1;
    public int trainedNode[][];

    private int rows;
    private int cols;
    private int dimension;
    private int initialValue;

    private Random rand;

    public SelfOrganisingMaps(String descriptor) {
        try {
            rows = Integer.parseInt(InitialParameter.getValue("sofm.rows"));
            cols = Integer.parseInt(InitialParameter.getValue("sofm.cols"));
            dimension = Integer.parseInt(InitialParameter.getValue(descriptor + ".dim"));
            initialValue = Integer.parseInt(InitialParameter.getValue(descriptor + ".initial"));
        } catch (Exception e) {
            System.out.println("ERROR: " + e);
            e.printStackTrace();
        }

        rand = new Random(109876L);

        inpWt = new double[rows][cols][dimension];
        distance = new double[rows][cols];
        inp = new double[dimension];
        trainedNode = new int[rows][cols];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                for (int k = 0; k < dimension; k++) {
                    inpWt[i][j][k] = (double) (rand.nextInt(initialValue));
                }
                distance[i][j] = 3.0;
            }
        }
        winNodeValue = 2.0;
        winNode0 = 0;
        winNode1 = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                trainedNode[i][j] = 0;
            }
        }
    }

    public void computeTest() {
        winNodeValue = 1000.0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                temp = 0.0;
                if (trainedNode[i][j] == 1 || trainedNode[i][j] == 2) {
                    for (int k = 0; k < dimension; k++) {
                        temp += Math.pow((inpWt[i][j][k] - inp[k]), 2);
                    }
                    if (Math.sqrt(temp) < winNodeValue) {
                        winNodeValue = Math.sqrt(temp);
                        winNode0 = i;
                        winNode1 = j;
                    }
                }
            }
        }
    }
}
