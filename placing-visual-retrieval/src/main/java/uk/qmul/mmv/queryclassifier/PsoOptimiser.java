package uk.qmul.mmv.queryclassifier;

import java.util.Random;
import java.lang.Math;

import uk.qmul.mmv.utils.InitialParameter;

public class PsoOptimiser {
	private double particles[][];
	private double fun[];
	private double pbest[][];
	public double gbest[];
	private double velocity[][];
	private double data[][];
	public double bestFit, globalFit;
	//private double gbestFit;

	private int dimension;
	private int initialValue;
	private int psoNumber;
	int index;
	
	private Random rand;

	public PsoOptimiser(String descriptor, double ini[], double y[]) {
            //System.out.println("PSO Optimiser: "+ini.length+" "+y.length);
		try {
			dimension=Integer.parseInt(InitialParameter.getValue(descriptor+".dim"));
			initialValue=Integer.parseInt(InitialParameter.getValue(descriptor+".initial"));
			psoNumber=Integer.parseInt(InitialParameter.getValue("pso.number"));
		} catch(Exception e) {
			System.out.println("ERROR: "+e);
			e.printStackTrace();
		}
		
		rand=new Random(109876L);
		
		particles=new double[dimension][psoNumber];
		fun=new double[dimension];
		pbest=new double[dimension][psoNumber];
		gbest=new double[dimension];
		velocity=new double[dimension][psoNumber];
		data=new double [dimension][psoNumber];

		for(int i=0; i<dimension; i++) {
			particles[i][0]=ini[i];
			pbest[i][0]=ini[i];
			gbest[i]=ini[i];
			fun[i]=y[i];
		}
		for(int i=0; i<dimension; i++) {
			for(int k=1; k<psoNumber; k++) {
				particles[i][k] = (double)(rand.nextInt(initialValue));
				pbest[i][k]=particles[i][k];
			}
		}
		for(int i=0; i<dimension; i++) {
			for(int k=0;k<psoNumber; k++) {
				velocity[i][k]=0.0;
			}
		}
		globalFit=1000.0;
	}

	public void personalBestCld() {
		for(int i=1; i<dimension; i++) {
			for(int j=0; j<psoNumber; j++) {
				if((pbest[i][j]-fun[i]) > data[i][j]) {
					pbest[i][j]=particles[i][j];
				}
			}
		}
	}

	public void globalBestCld() {
		functionEvalutionCld();
		double min;
		for(int i=0; i<dimension; i++) {
			min=data[i][0];
			for(int j=1; j<psoNumber; j++) {
				if(min>data[i][j]) {
					gbest[i]=particles[i][j];
					min=data[i][j];
				}
			}
		}
	}

	private void velocityUpdateCld() {
		for(int i=0; i<dimension; i++) {
			for(int j=0; j<psoNumber; j++) {
				velocity[i][j] = velocity[i][j] + (0.32 * (pbest[i][j]-particles[i][j])) + (0.68 * (gbest[i]-particles[i][j]));
			}
		}
	}

	private void positionUpdateCld() {
		for(int i=0; i<dimension; i++) {
			for(int j=0; j<psoNumber; j++) {
				particles[i][j] = particles[i][j] + velocity[i][j];
			}
		}
	}

	private void functionEvalutionCld() {
		//double tempV;
		//tempV=0.0;
		for(int i=0; i<dimension; i++) {
			for(int k=0; k<psoNumber; k++) {
				data[i][k] = Math.abs((fun[i]-particles[i][k]));
			}
		}
	}

	public void updateNeuronCld(double threshold) {
		double temp;
		int cou=0;
		do {
			globalBestCld();
			personalBestCld();
			velocityUpdateCld();
			positionUpdateCld();
			cou += 1;
			temp=0.0;
			for(int i=0; i<dimension; i++) {
				for(int j=0; j<psoNumber; j++) {
					temp +=(gbest[i]-particles[i][j]);
				}
			}
			temp=temp/dimension;
			cou++;
		} while(Math.abs(temp) > threshold && cou<100000);
	}
}
