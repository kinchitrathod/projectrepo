/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.qmul.mmv.utils;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;

/**
 *
 * @author KINCHIT RATHOD
 */
public class ProcessFeaturesFile {

    private static final String INPUT_CLUSTERS = "E:/PlacingTaskData/ClusterFiles/Clusters.txt";
    private static final String INPUT_FEATUREFILE = "E:/PlacingTaskData/ImageFeaturesMerged/ImageFeature6COLExtracted.txt";
    private static final String OUTPUT_FEATUREFILE = "E:/PlacingTaskData/COLFeaturesRefined6New.txt";

    public static void main(String args[]) {

        try {

            BufferedReader clusterReaderQuery = new BufferedReader(new FileReader(INPUT_CLUSTERS));

            StringBuilder str = new StringBuilder();
            int count = 0;
            String line = null;
            while ((line = clusterReaderQuery.readLine()) != null) {
                str.append(line).append(",");
                count++;
            }

            //System.out.println(count);
            FileOutputStream outputFile = new FileOutputStream(OUTPUT_FEATUREFILE);
            PrintStream printOutputFile = new PrintStream(outputFile);

            BufferedReader features = new BufferedReader(new FileReader(INPUT_FEATUREFILE));
            String lineStr = null;
            while ((lineStr = features.readLine()) != null) {
                if (str.toString().contains(lineStr.substring(0, lineStr.indexOf(" ")))) {
                    printOutputFile.println(lineStr);
                }
            }
        } catch (Exception e) {

        }

    }
}
