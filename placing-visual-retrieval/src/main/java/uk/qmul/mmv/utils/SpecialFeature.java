/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.qmul.mmv.utils;


public class SpecialFeature {

    public static double[] convertStringDouble(String str, int dim) {

        double[] value=new double[dim];
      
        String[] features=str.split(" ");
        int count=0;
        for(String feature:features) {
            value[count]=Double.parseDouble(feature);
            count++;
        }
        
        return value;

    }
}
