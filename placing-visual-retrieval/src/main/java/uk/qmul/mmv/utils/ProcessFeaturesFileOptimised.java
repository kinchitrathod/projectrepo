/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.qmul.mmv.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KINCHIT RATHOD
 */
public class ProcessFeaturesFileOptimised {
    
    private static final String CLUSTER_IMAGESSTATSONLY = "E:/PlacingTaskData/ClusterFiles/ClusterImagesStatsOnly.txt";
    private static final String INPUT_CLUSTERIMAGEFILE = "E:/PlacingTaskData/ClusterFiles/ClusterImagesOnly.txt";
    private static final String INPUT_FEATUREFILE = "E:/PlacingTaskData/ImageFeaturesMerged/ImageFeature6EHDExtracted.txt";
    private static final String OUTPUT_FEATUREREFINED= "E:/PlacingTaskData/EHDFeaturesRefined14-2-2014.txt";
    
    public static void main(String args[]) {

         boolean success1 = (new File(OUTPUT_FEATUREREFINED)).delete();
        if (success1) 
            System.out.println("File Deleted!");
        
        try {

            BufferedReader clusterReaderQuery = new BufferedReader(new FileReader(INPUT_CLUSTERIMAGEFILE));
            StringBuilder str = new StringBuilder();
            String line = null;
            List<String> imageList = new ArrayList<>();
            int count=0;
            
            FileOutputStream fileOutputImagesStatsOnly = new FileOutputStream(CLUSTER_IMAGESSTATSONLY, true);
            PrintStream outputPrintImagesStats = new PrintStream(fileOutputImagesStatsOnly);
      
            int i =1;
            while ((line = clusterReaderQuery.readLine()) != null) {
            
                String imageSplits[] = line.split(" ");
                for (String imageSplit : imageSplits)
                {
                    imageList.add(imageSplit);
                    count++;
                }
                outputPrintImagesStats.println(i + " " +count);
                count=0;
                i++;
            }
           
            System.out.println("ImageList Size:"+imageList.size());
            System.out.println();
            
            FileOutputStream outputFile = new FileOutputStream(OUTPUT_FEATUREREFINED);
            PrintStream printOutputFile = new PrintStream(outputFile);

            BufferedReader features = new BufferedReader(new FileReader(INPUT_FEATUREFILE));
            String lineStr = null;
            while ((lineStr = features.readLine()) != null) 
            {
                String ImageID= lineStr.substring(0, lineStr.indexOf(" "));
                if(imageList.contains(ImageID))
                {
                    //System.out.println("Yes");
                    imageList.remove(ImageID);
                    printOutputFile.println(lineStr);
                    //System.out.println(imageList.size());
                    
                }
            }
            
             for (int n = 0; n < 3; n++)
            {
                try {
                    Thread.sleep(300);
                    Beep.makeasound();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeoTaggedFinder.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        } catch (Exception e) {

        }

    }
}
