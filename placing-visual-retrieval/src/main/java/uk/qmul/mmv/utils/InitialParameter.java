package uk.qmul.mmv.utils;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class InitialParameter {
	private static ResourceBundle resource=null;

	private static String resourceName="uk.qmul.mmv.ApplicationResources";

	public static void setResourceName(String name) throws MissingResourceException {
		resourceName = name;
		loadProperties();
	}


	public static String getResourceName() {
		return resourceName;
	}


	private static void loadProperties() throws MissingResourceException {
			resource = ResourceBundle.getBundle(resourceName);
	}


	public static String getValue(String key) throws IllegalArgumentException {
		if(resource==null) {
			loadProperties();
		}

		String value="";

		try {
			value=resource.getString(key);
		} catch(MissingResourceException mre) {
			if(mre.getKey()!=null) {
				value="";
			}
		} catch(NullPointerException npe) {
			if(resourceName == null)
				throw new IllegalArgumentException("Resource Name not Specified");
		}

		return value;
	}
}
